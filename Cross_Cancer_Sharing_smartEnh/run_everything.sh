
export files=""

rm peak_counts.txt genome_in_enhs.txt

samtools view -h /archive/tony/TONY2/George/Homo.sapiens/Neuroblastoma/Kelly/H3K27Ac/20141121_3371/20141121_3371_hg19.sorted.bam| head -n 27 > header

for file in Kelly_George Neuroblastoma_NB1 Neuroblastoma_NB2 Neuroblastoma_NB3 SHSY5Y_Look SKNSH BE2 BT_549 HCC1954 MCF7 MDA_MB_468 T47D ZR_75_1 MDA_MB_231 DND41 Jurkat_JR1 Loucy MOLT4 RPMI-8402 DU528 P12_ICHIKAWA KOPTK1 PF382 CCRFCEM CUTLL1 GLC16 H2171 H69 H82 HCT116 HT29 VACO_400 VACO_503 VACO_9M LS174T Colo741 MM034_Primary_Melanoma MM047_Primary_Melanoma MM087_Primary_Melanoma MM001_Primary_Melanoma MM057_Primary_Melanoma MM099_Primary_Melanoma MM011_Primary_Melanoma MM118_Primary_Melanoma MM031_Primary_Melanoma MM074_Primary_Melanoma P265_Primary_DLBCL P286_Primary_DLBCL DHL6 HBL1 LY3 LY4 TOLEDO P397_Primary_DLBCL P448_Primary_DLBCL GM12878 HeLa HepG2 K562 LnCAP MM1S panc1 u87 U2OS A673 SKMNC P493 PC3 V459 SKMEL5_Aerts Ly1_Published GBM_S08 GBM_R28 GBM_P69 GBM_2585 GBM_2493 GBM_B39 GBM_IDHmut5661 HPAF2 PT45P1 CFPAC1 MIAPaca2 CAPAN2 CAPAN1 CAL51 DOHH2 GRANTA_519 KARPAS_422 MINO OCI_LY7 PFEIFFER REC1 NMC_1015 JEKO1 797_r1_1  RS411 MOLM14 HUCCT1 HCC95 H358_r2 MCF10A  LS180 #92

do
  echo $file
  export readFile="../CANCERS/$file.all_chroms.psl.real.filt.sam"
  export scaFile="../Scaffold_Dev/$file.all_chroms.psl.real.filt.sam"

  cat $readFile $scaFile | grep -v "chrM" > $file.tmp.sam
  cat header $file.tmp.sam > $file.sam
  samtools view -b $file.sam > $file.bam

  intersectBed -a $file.bam -b ../Enhancers/$file\_peaks.bed > $file.inPeaks.bam
  samtools view $file.inPeaks.bam > $file.inPeaks.sam
#
  cat header $readFile > $file.read.sam
  samtools view -b $file.read.sam > $file.read.bam

  intersectBed -a $file.read.bam -b ../Enhancers/$file\_peaks.bed > $file.read.inPeaks.bam
  samtools view $file.read.inPeaks.bam > $file.read.inPeaks.sam
#
  cat header $scaFile > $file.sca.sam
  samtools view -b $file.sca.sam > $file.sca.bam

  intersectBed -a $file.sca.bam -b ../Enhancers/$file\_peaks.bed > $file.sca.inPeaks.bam
  samtools view $file.sca.inPeaks.bam > $file.sca.inPeaks.sam

  perl filter_sam_by_string.pl $file.inPeaks.sam > $file.inPeaks.bed
  perl filter_sam_by_string.pl $file.tmp.sam > $file.bed
  perl filter_sam_by_string.pl $file.read.sam > $file.read.bed
  perl filter_sam_by_string.pl $file.sca.sam > $file.sca.bed
  perl filter_sam_by_string.pl $file.sca.inPeaks.sam > $file.sca.inPeaks.bed
  perl filter_sam_by_string.pl $file.read.inPeaks.sam > $file.read.inPeaks.bed

# 
  awk -F\\t '{print $1 "_" $2 "_" $3 "_" $4}' $file.inPeaks.bed | sort | uniq > $file.inPeaks.unique.txt
  awk -F\\t '{print $1 "_" $2 "_" $3 "_" $4}' $file.inPeaks.bed | sort | uniq | sed s/\_/\\t/g > $file.inPeaks.unique.bed
  awk -F\\t '{print $1 "_" $2 "_" $3 "_" $4}' $file.bed | sort | uniq > $file.unique.txt
  awk -F\\t '{print $1 "_" $2 "_" $3 "_" $4}' $file.bed | sort | uniq | sed s/\_/\\t/g > $file.unique.bed
  awk -F\\t '{print $1 "_" $2 "_" $3 "_" $4}' $file.read.bed | sort | uniq | sed s/\_/\\t/g > $file.read.unique.bed
  awk -F\\t '{print $1 "_" $2 "_" $3 "_" $4}' $file.sca.bed | sort | uniq | sed s/\_/\\t/g > $file.sca.unique.bed


  rm $file.sam $file.inPeaks.bed $file.inPeaks.bam $file.bam $file.tmp $file.inPeaks.sam $file.tmp.sam

  wc -l ../CANCERS/$file\_peaks.bed >> peak_counts.txt
  export size=$(awk -F\\t '{sum += $3-$2} END {print sum}' ../CANCERS/$file\_peaks.bed)
  echo -e "$file\t$size" >> genome_in_enhs.txt

  inPeaksFiles=$(echo $inPeaksFiles $file.inPeaks.unique.txt)
  AllFiles=$(echo $AllFiles $file.unique.txt)

done

cat $inPeaksFiles | sed s/\\t/\_/g | sort | uniq | sed s/\_/\\t/g > all_inPeaks_inserts.bed
cat $inPeaksFiles | sed s/\\t/\_/g | sort | uniq > all_inPeaks_inserts.txt
cat $inPeaksFiles > all_inPeaks_inserts_gross.txt
cat $AllFiles | sed s/\\t/\_/g | sort | uniq | sed s/\_/\\t/g > all_inserts.bed
cat $AllFiles | sed s/\\t/\_/g | sort | uniq > all_inserts.txt

export files3=$(echo $AllFiles | sed s/\ /\,/g)
export files3Peaks=$(echo $inPeaksFiles | sed s/\ /\,/g)


bsub -K -q special perl count_line_occurrences_across_files.pl all_inPeaks_inserts.txt $files3 \> all_inPeaks_inserts_acrossAll.txt
bsub -K -q special perl count_line_occurrences_across_files.pl all_inPeaks_inserts.txt $files3Peaks \> all_inPeaks_inserts_acrossAllInPeaks.txt

wait

R --vanilla < parse_sharing.R


cat *.read.unique.bed | awk -F\\t '{print $0 "\t" length($4)}' | sort -k5,5n >| cat_all_read.unique.bed
cat *.sca.unique.bed | awk -F\\t '{print $0 "\t" length($4)}' | sort -k5,5n >| cat_all_sca.unique.bed

cat *.notUbiq.bed > notUbiq_cat.bed

