THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE WHITEHEAD INSTITUTE FOR BIOMEDICAL RESEARCH BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Software has several dependencies to function including samtools, bedtools, bowtie, bowtie2, blat, fastaFromBed

Aspects of the code may not function identically across setups, and customization is recommended.  Specific attention should be paid to job submission, as this code was developed in an LSF environment

Points of contact are Brian Abraham (abraham@wi.mit.edu) and Richard A. Young (young@wi.mit.edu)

The most recent version of the code is available at http://bitbucket.org/young_computation/

run_everything.sh files need to be executed

The rough order of execution is:
-CANCERS
-Scaffold_Dev/Unequal_Read_Length_Tests (if data have unequal read lengths)
-Scaffold_Dev
-Enhancers
-Cross_Cancer_Sharing_smartEnh
-dbSNP
-Custom_Genomes
-Gene_Assignment_smartEnh_B2
-Compile_Mega_EAI_Table



In Gene_Assignment_smartEnh_B2, TableS2A.txt comes from PMID: 26940867
Oncogenes.txt is a list of gene names with one name per line and comes from COSMIC
hg19_AllSNPs144.ucsc.gz comes from the UCSC genome browser
