declare -A k27ac=(

["MM034_Primary_Melanoma"]="/archive/tony/TONY2/Aerts/Homo.sapiens/Melanoma/Primary.Melanoma.Culture/H3K27Ac/20150305_4243/20150305_4243_hg19.sorted.bam"
["MM047_Primary_Melanoma"]="/archive/tony/TONY2/Aerts/Homo.sapiens/Melanoma/Primary.Melanoma.Culture/H3K27Ac/20150305_4244/20150305_4244_hg19.sorted.bam"
["MM087_Primary_Melanoma"]="/archive/tony/TONY2/Aerts/Homo.sapiens/Melanoma/Primary.Melanoma.Culture/H3K27Ac/20150305_4247/20150305_4247_hg19.sorted.bam"
["MM001_Primary_Melanoma"]="/archive/tony/TONY2/Aerts/Homo.sapiens/Melanoma/Primary.Melanoma.Culture/H3K27Ac/20150305_4240/20150305_4240_hg19.sorted.bam"
["MM057_Primary_Melanoma"]="/archive/tony/TONY2/Aerts/Homo.sapiens/Melanoma/Primary.Melanoma.Culture/H3K27Ac/20150305_4245/20150305_4245_hg19.sorted.bam"
["SKMEL5_Aerts"]="/archive/tony/TONY2/Aerts/Homo.sapiens/Melanoma/SK-MEL-5/H3K27Ac/20150305_3803/20150305_3803_hg19.sorted.bam"
["MM099_Primary_Melanoma"]="/archive/tony/TONY2/Aerts/Homo.sapiens/Melanoma/Primary.Melanoma.Culture/H3K27Ac/20150305_4248/20150305_4248_hg19.sorted.bam"
["SKMEL5_Tsao"]="/archive/tony/TONY2/Tsao/Homo.sapiens/Melanoma/SK-MEL-5/H3K27Ac/20150318_3621/20150318_3621_hg19.sorted.bam"
["MM011_Primary_Melanoma"]="/archive/tony/TONY2/Aerts/Homo.sapiens/Melanoma/Primary.Melanoma.Culture/H3K27Ac/20150305_4241/20150305_4241_hg19.sorted.bam"
["MM118_Primary_Melanoma"]="/archive/tony/TONY2/Aerts/Homo.sapiens/Melanoma/Primary.Melanoma.Culture/H3K27Ac/20150305_4249/20150305_4249_hg19.sorted.bam"
["MM031_Primary_Melanoma"]="/archive/tony/TONY2/Aerts/Homo.sapiens/Melanoma/Primary.Melanoma.Culture/H3K27Ac/20150305_4242/20150305_4242_hg19.sorted.bam"
["MM074_Primary_Melanoma"]="/archive/tony/TONY2/Aerts/Homo.sapiens/Melanoma/Primary.Melanoma.Culture/H3K27Ac/20150305_4246/20150305_4246_hg19.sorted.bam"


)

for file in "${!k27ac[@]}"
do
  echo $file

  ########################
  #### CODE BLOCK 1 ##### 
  ########################
  samtools view -b -f 4 ${k27ac[$file]} | bamToFastq -i /dev/stdin -fq $file.unmapped.fastq
  mkdir $file.unmapped
  awk -F\\t '{ line=line+1 
    if(line % 4==1) print $0
    else if(line % 4==2) print substr($0, 0, 25)
    else if(line % 4==3) print $0
    else if(line % 4==0) print substr($0, 0, 25)
  }' $file.unmapped.fastq > $file.unmapped.trunc31.fastq

  bsub -q special -n 12 -R "span[hosts=1]" edena -nThreads 12 -t 25 -r $file.unmapped.trunc31.fastq -p $file.unmapped/$file.unmapped


  #########################
  ##### CODE BLOCK 2 ##### 
  #########################

  bsub -q special -n 12 -R "span[hosts=1]" edena -nThreads 12 -d 20 -c 20 -minCoverage 5 -e $file.unmapped/$file.unmapped.ovl -p $file.unmapped/$file.unmapped
done

cp -R *unmapped ..