declare -A types=(
["Jurkat_JR1"]="1_TALL"
["MOLT4"]="1_TALL"
["MOLT13"]="1_TALL"
["PF382"]="1_TALL"
["DU528"]="1_TALL"
["P12_ICHIKAWA"]="1_TALL"
["PEER"]="1_TALL"
["CUTLL1"]="1_TALL"
["CCRFCEM"]="1_TALL"
["KOPTK1"]="1_TALL"
["Loucy"]="1_TALL"
["DND41"]="1_TALL"
["RPMI-8402"]="1_TALL"
["Jurkat"]="1_TALL"
["Jurkat_JR2"]="3_TALL"
["MV411_2"]="1_TALL"
["MDA_MB_231"]="2_Breast"
["MDA_MB_468"]="2_Breast"
["ZR_75_1"]="2_Breast"
["T47D"]="2_Breast"
["BT_549"]="2_Breast"
["HCC1954"]="2_Breast"
["MCF7"]="2_Breast"
["BE2C_only"]="3_Neuroblastoma"
["SKNSH"]="3_Neuroblastoma"
["SHSY5Y_Look"]="3_Neuroblastoma"
["EBC1"]="3_Neuroblastoma"
["BE2"]="3_Neuroblastoma"
["NGP"]="3_Neuroblastoma"
["NB1643"]="3_Neuroblastoma"
["CHP134"]="3_Neuroblastoma"
["Neuroblastoma_NB3"]="3_Neuroblastoma"
["Kelly_George"]="3_Neuroblastoma"
["Neuroblastoma_NB2"]="3_Neuroblastoma"
["Neuroblastoma_NB1"]="3_Neuroblastoma"
["SHSY5Y_George"]="3_Neuroblastoma"
["HCC827_GR6_K27ac"]="4_Lung"
["HCC827_K27ac"]="4_Lung"
["H2171"]="4_Lung"
["H69"]="4_Lung"
["H82"]="4_Lung"
["GLC16"]="4_Lung"
["PC9_DMSO"]="4_Lung"
["V459"]="5_Colorectal"
["Colo741"]="5_Colorectal"
["HT29"]="5_Colorectal"
["LS174T"]="5_Colorectal"
["DLD1"]="5_Colorectal"
["HCT116"]="5_Colorectal"
["VACO_503"]="5_Colorectal"
["VACO_400"]="5_Colorectal"
["VACO_9M"]="5_Colorectal"
["MM057_Primary_Melanoma"]="6_Melanoma"
["MM001_Primary_Melanoma"]="6_Melanoma"
["MM011_Primary_Melanoma"]="6_Melanoma"
["SKMEL5_Aerts"]="6_Melanoma"
["MM047_Primary_Melanoma"]="6_Melanoma"
["MM034_Primary_Melanoma"]="6_Melanoma"
["MM074_Primary_Melanoma"]="6_Melanoma"
["MM099_Primary_Melanoma"]="6_Melanoma"
["MM031_Primary_Melanoma"]="6_Melanoma"
["MM087_Primary_Melanoma"]="6_Melanoma"
["MM118_Primary_Melanoma"]="6_Melanoma"
["SKMEL5_Tsao"]="6_Melanoma"
["LY4"]="9_Bcelllymphoma"
["DHL6"]="9_Bcelllymphoma"
["LY3"]="9_Bcelllymphoma"
["P265_Primary_DLBCL"]="9_Bcelllymphoma"
["P448_Primary_DLBCL"]="9_Bcelllymphoma"
["TOLEDO"]="9_Bcelllymphoma"
["HBL1"]="9_Bcelllymphoma"
["P286_Primary_DLBCL"]="9_Bcelllymphoma"
["P397_Primary_DLBCL"]="9_Bcelllymphoma"
["PC3"]="11_Other"
["u87"]="8_Glioblastoma"
["Ly1"]="9_Bcelllymphoma"
["MM1S"]="11_Other"
["P493"]="11_Other"
["HeLa"]="11_Other"
["MOLM13"]="11_Other"
["MV411_1"]="11_Other"
["GM12878"]="11_Other"
["HepG2"]="11_Other"
["SEM"]="11_Other"
["K562"]="11_Other"
["A673"]="11_Other"
["panc1"]="10_Pancreatic"
["SKMNC"]="11_Other"
["LnCAP"]="11_Other"
["U2OS"]="11_Other"



["Ly1_Published"]="9_Bcelllymphoma"
["BE2C_only_new"]="3_Neuroblastoma"


["CAL51"]="2_Breast"
["NMC_1015"]="11_Other"
["PFEIFFER"]="9_Bcelllymphoma"
["GBM_S08"]="8_Glioblastoma"
["GBM_R28"]="8_Glioblastoma"
["GBM_P69"]="8_Glioblastoma "
["GBM_2585"]="8_Glioblastoma"
["GBM_2493"]="8_Glioblastoma"
["GBM_B39"]="8_Glioblastoma"
["GBM_IDHmut5661"]="8_Glioblastoma"

["KARPAS_422"]="9_Bcelllymphoma "
["JEKO1"]="11_Other"
["MINO"]="9_Bcelllymphoma "
["797_r1_1"]="11_Other"
["797_r1_2"]="11_Other "
["DOHH2"]="9_Bcelllymphoma "
["GRANTA_519"]="9_Bcelllymphoma"
["OCI_LY7"]="9_Bcelllymphoma"
["REC1"]="9_Bcelllymphoma"
["RS411"]="11_Other"

["MOLM14"]="11_Other "
["HUCCT1"]="11_Other "
["HCC95"]="4_Lung"
["H2009_r1"]="4_Lung"
["H2009_r2"]="4_Lung "
["H2009_empty"]="4_Lung "
["H2009_control"]="4_Lung"
["H358_r1"]="4_Lung"
["H358_r2"]="4_Lung"
["MCF10A"]="2_Breast"

["HPAF2"]="10_Pancreatic"
["PT45P1"]="10_Pancreatic"
["CFPAC1"]="10_Pancreatic"
["MIAPaca2"]="10_Pancreatic"
["CAPAN2"]="10_Pancreatic"
["CAPAN1"]="10_Pancreatic"
["LS180"]="5_Colorectal"


)



for file in Kelly_George Neuroblastoma_NB1 Neuroblastoma_NB2 Neuroblastoma_NB3 SHSY5Y_Look SKNSH BE2 BT_549 HCC1954 MCF7 MDA_MB_468 T47D ZR_75_1 MDA_MB_231 DND41 Jurkat_JR1 Loucy MOLT4 RPMI-8402 DU528 P12_ICHIKAWA KOPTK1 PF382 CCRFCEM CUTLL1 GLC16 H2171 H69 H82 HCT116 HT29 VACO_400 VACO_503 VACO_9M LS174T Colo741 MM034_Primary_Melanoma MM047_Primary_Melanoma MM087_Primary_Melanoma MM001_Primary_Melanoma MM057_Primary_Melanoma MM099_Primary_Melanoma MM011_Primary_Melanoma MM118_Primary_Melanoma MM031_Primary_Melanoma MM074_Primary_Melanoma P265_Primary_DLBCL P286_Primary_DLBCL DHL6 HBL1 LY3 LY4 TOLEDO P397_Primary_DLBCL P448_Primary_DLBCL GM12878 HeLa HepG2 K562 LnCAP MM1S panc1 u87 U2OS A673 SKMNC P493 PC3 V459 SKMEL5_Aerts Ly1_Published  GBM_S08 GBM_R28 GBM_P69 GBM_2585 GBM_2493 GBM_B39 GBM_IDHmut5661 HPAF2 PT45P1 CFPAC1 MIAPaca2 CAPAN2 CAPAN1 CAL51 DOHH2 GRANTA_519 KARPAS_422 MINO OCI_LY7 PFEIFFER REC1 NMC_1015 JEKO1 797_r1_1  RS411 MOLM14 HUCCT1 HCC95 H358_r2 MCF10A LS180
do
  export fileName=$(echo $file | sed s/\-/\./)
  ls ../Cross_Cancer_Sharing_DELs_smartEnh/$file.inPeaks.unique.txt

############# COUNT APPEARANCES ACROSS SAMPLES
  cp ../Cross_Cancer_Sharing_DELs_smartEnh/$file.inPeaks.unique.txt $file.uniqueEAD.txt
  export columns=$(awk -F\\t '{print NF-1; exit}' ../Cross_Cancer_Sharing_smartEnh/all_insertions_w_Sample_counts.txt)
  perl vlookup.pl $file.uniqueEAD.txt 0 ../Cross_Cancer_Sharing_DELs_smartEnh/all_insertions_w_Sample_counts.txt 0 $columns $file.uniqueEAD.samplesIn.txt

########### IN DBSNP
  perl vlookup.pl $file.uniqueEAD.samplesIn.txt 0 ../dbSNP_DELs/indbSNP_gross_cat.txt 0 0 temp
  awk -F\\t '{
    if($3 != "XXX")
      print $1 "\t" $2 "\t1"
    else
      print $1 "\t" $2 "\t0"
    }' temp > $file.uniqueEAD.samplesIn.indbSNP.txt

########## ASSIGN GENES
  sed s/\:/\_/ ../Gene_Assignment_DELs_smartEnh/$fileName.all.txt | sed s/\-/\_/ | sed s/\ /\,/g > $file.allGenesInIN.txt
  perl vlookup.pl $file.uniqueEAD.samplesIn.indbSNP.txt 0 $file.allGenesInIN.txt 0 4  temp
  perl vlookup.pl temp 0 $file.allGenesInIN.txt 0 8 temp2
  perl vlookup.pl temp2 0 $file.allGenesInIN.txt 0 9 temp3 

  awk -F\\t '{
    if($6=="-1")
      $6="NoOncogene"

    if($5=="XXX") {
      $5="NoGene"
      $6="NoGene"
    }

    if($5=="-1") {
      $5="NoGene"
      $6="NoGene"
    }

    if($4=="XXX" || $4==".") {
      $4="NoI.N."
      $5="NoI.N."
      $6="NoI.N."
    }
    print $0
    }' temp3 | sed s/\ /\\t/g > $file.uniqueEAD.samplesIn.indbSNP.INinfo.txt

########### READ BIASING

  awk -F' ' '{print $4 "_" $5 "_" $6 "_" $7 "\t" $1 "\t" $2 "\t" $3}' ../Custom_Genomes/Auto_All_Cancer_smartEnh_FIXED_ALL_B2_DELs/counts.$fileName.txt > $file.readCounts.txt
  perl vlookup.pl $file.uniqueEAD.samplesIn.indbSNP.INinfo.txt 0 $file.readCounts.txt 0 1 temp
  perl vlookup.pl temp 0 $file.readCounts.txt 0 2 temp2
  perl vlookup.pl temp2 0 $file.readCounts.txt 0 3 temp4
  perl vlookup.pl temp4 0 $file.readCounts.txt 0 4 $file.uniqueEAD.samplesIn.indbSNP.INinfo.readInfo.txt

###########

  awk -v sample=$file -v type=${types[$file]} -F\\t '{print sample "\t" type "\t" $0}' $file.uniqueEAD.samplesIn.indbSNP.INinfo.readInfo.txt > $file.table.txt

done

echo -e "SAMPLE\tSAMPLETYPE\tLOCUSID\tSAMPLESIN\tINdbSNP\tI.N.COORDS\tI.N.GENES\tI.N.ONCOGENES\tROWNUM\tREFREADS\tINSREADS" > header
cat header *.table.txt > MegaTable.txt



awk -F\\t '{print $1}' MegaTable.txt > SampleIDs.txt

sed -i s/Jurkat\_JR1/Jurkat/ SampleIDs.txt
sed -i s/DU528/DU\.528/ SampleIDs.txt
sed -i s/PF382/PF\-382/ SampleIDs.txt
sed -i s/KOPTK1/KOPT\-K1/ SampleIDs.txt
sed -i s/CCRFCEM/CCRF\-CEM/ SampleIDs.txt
sed -i s/P12\_ICHIKAWA/P12\-ICHIKAWA/ SampleIDs.txt
sed -i s/MDA\_MB\_231/MDA\-MB\-231/ SampleIDs.txt
sed -i s/MDA\_MB\_468/MDA\-MB\-468/ SampleIDs.txt
sed -i s/ZR\_75\_1/ZR\-75\-1/ SampleIDs.txt
sed -i s/BT\_549/BT\-549/ SampleIDs.txt
sed -i s/BE2C\_only/BE2C/ SampleIDs.txt
sed -i s/SKNSH/SK\-N\-AS/ SampleIDs.txt
sed -i s/SHSY5Y\_Look/SH\-SY5Y/ SampleIDs.txt
sed -i s/Kelly\_George/Kelly/ SampleIDs.txt
sed -i s/HCC827\_K27ac/HCC827/ SampleIDs.txt
sed -i s/VACO\_400/VACO400/ SampleIDs.txt
sed -i s/VACO\_9M/VACO9M/ SampleIDs.txt
sed -i s/VACO\_503/VACO503/ SampleIDs.txt
sed -i s/\_Primary\_Melanoma/\ Primary\ Melanoma/ SampleIDs.txt
sed -i s/\_Primary\_DLBCL/\ Primary\ DLBCL/ SampleIDs.txt
sed -i s/MV411\_1/MV411/ SampleIDs.txt
sed -i s/Neuroblastoma\_/Neuroblastoma\ / SampleIDs.txt
sed -i s/SKMEL5\_Aerts/SK\-MEL\-5/ SampleIDs.txt
sed -i s/SKMNC/SK\-N\-MC/ SampleIDs.txt
sed -i s/Ly1/LY1/ SampleIDs.txt
sed -i s/797\_r1\_1/TC797/ SampleIDs.txt
sed -i s/BE2C\_new/BE2C/ SampleIDs.txt
sed -i s/CAPAN1/CAPAN\-1/ SampleIDs.txt
sed -i s/CAPAN2/CAPAN\-2/ SampleIDs.txt
sed -i s/GBM\_/GBM\ / SampleIDs.txt
sed -i s/GRANTA\_519/GRANTA\-519/ SampleIDs.txt
sed -i s/H358\_r2/H358/ SampleIDs.txt
sed -i s/JEKO1/JeKo1/ SampleIDs.txt
sed -i s/KARPAS_422/KARPAS\ 422/ SampleIDs.txt
sed -i s/LY1_Published/LY1/ SampleIDs.txt
sed -i s/MIAPaca2/MIA\ PaCa2/ SampleIDs.txt
sed -i s/SAMPLE/DISPLAYSAMPLE/ SampleIDs.txt
sed -i s/NMC\_1015/1015c/ SampleIDs.txt
sed -i s/OCI\_LY7/LY7/ SampleIDs.txt
sed -i s/PT45P1/PT45\-P1/ SampleIDs.txt
sed -i s/REC1/REC\-1/ SampleIDs.txt
sed -i s/RS411/RS\;411/ SampleIDs.txt
sed -i s/V459/V429/ SampleIDs.txt

paste MegaTable.txt SampleIDs.txt > MegaTable2.txt


export date=$(date +"%m-%d-%y")
cp MegaTable2.txt MegaTable_$date.txt

