
zgrep "insertion" hg19_AllSNPs144.ucsc.gz | gzip > hg19_AllSNPs144.insertions.ucsc.gz
gunzip hg19_AllSNPs144.insertions.ucsc.gz
perl parse_dbSNP_multiSeqSamePos.pl >| hg19_AllSNPs144.insertions.txt

rm indbSNP_summary.txt

for file in Kelly_George Neuroblastoma_NB1 Neuroblastoma_NB2 Neuroblastoma_NB3 SHSY5Y_Look SKNSH BE2 BT_549 HCC1954 MCF7 MDA_MB_468 T47D ZR_75_1 MDA_MB_231 DND41 Jurkat_JR1 Loucy MOLT4 RPMI-8402 DU528 P12_ICHIKAWA KOPTK1 PF382 CCRFCEM CUTLL1 GLC16 H2171 H69 H82 HCT116 HT29 VACO_400 VACO_503 VACO_9M LS174T Colo741 MM034_Primary_Melanoma MM047_Primary_Melanoma MM087_Primary_Melanoma MM001_Primary_Melanoma MM057_Primary_Melanoma MM099_Primary_Melanoma MM011_Primary_Melanoma MM118_Primary_Melanoma MM031_Primary_Melanoma MM074_Primary_Melanoma P265_Primary_DLBCL P286_Primary_DLBCL DHL6 HBL1 LY3 LY4 TOLEDO P397_Primary_DLBCL P448_Primary_DLBCL GM12878 HeLa HepG2 K562 LnCAP MM1S panc1 u87 U2OS A673 SKMNC P493 PC3 V459 SKMEL5_Aerts Ly1_Published GBM_S08 GBM_R28 GBM_P69 GBM_2585 GBM_2493 GBM_B39 GBM_IDHmut5661 HPAF2 PT45P1 CFPAC1 MIAPaca2 CAPAN2 CAPAN1 CAL51 DOHH2 GRANTA_519 KARPAS_422 MINO OCI_LY7 PFEIFFER REC1 NMC_1015 JEKO1 797_r1_1  RS411 MOLM14 HUCCT1 HCC95 H358_r2 MCF10A LS180
do
  bsub "grep -w -f ../Cross_Cancer_Sharing_smartEnh/$file.inPeaks.unique.txt hg19_AllSNPs144.insertions.txt > $file.unique.indbSNPw.txt"
  grep -v -w -f $file.unique.indbSNPw.txt $file.notUbiq.txt > $file.notUbiq.notindbSNPw.txt
done


cat *.indbSNPw.*txt | sort | uniq > indbSNP_gross_cat.txt
