#!/usr/bin/perl -w

use strict;

my @lineArr;
my @insSeqs;
my $chrom;
my $position;

open(IN, "<hg19_AllSNPs144.insertions.ucsc");

while(<IN>) {
  chomp($_);
  @lineArr = split("\t", $_);

  $chrom = $lineArr[1];
  $position = $lineArr[2]+1;

  @insSeqs = split("/", $lineArr[9]);
  shift(@insSeqs);

  foreach my $insSeq (@insSeqs) {
    print $chrom . "_" . $position . "_" . $position . "_" . $insSeq . "\n";
  }


}


exit(0);