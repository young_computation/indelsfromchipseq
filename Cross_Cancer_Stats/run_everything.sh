declare -A types=(
["Jurkat_JR1"]="1_TALL"
["MOLT4"]="1_TALL"
["MOLT13"]="1_TALL"
["PF382"]="1_TALL"
["DU528"]="1_TALL"
["P12_ICHIKAWA"]="1_TALL"
["PEER"]="1_TALL"
["CUTLL1"]="1_TALL"
["CCRFCEM"]="1_TALL"
["KOPTK1"]="1_TALL"
["Loucy"]="1_TALL"
["DND41"]="1_TALL"
["RPMI-8402"]="1_TALL"
["Jurkat"]="1_TALL"
["Jurkat_JR2"]="3_TALL"
["MV411_2"]="1_TALL"
["MDA_MB_231"]="2_Breast"
["MDA_MB_468"]="2_Breast"
["ZR_75_1"]="2_Breast"
["T47D"]="2_Breast"
["BT_549"]="2_Breast"
["HCC1954"]="2_Breast"
["MCF7"]="2_Breast"
["BE2C_only"]="3_Neuroblastoma"
["SKNSH"]="3_Neuroblastoma"
["SHSY5Y_Look"]="3_Neuroblastoma"
["EBC1"]="3_Neuroblastoma"
["BE2"]="3_Neuroblastoma"
["NGP"]="3_Neuroblastoma"
["NB1643"]="3_Neuroblastoma"
["CHP134"]="3_Neuroblastoma"
["Neuroblastoma_NB3"]="3_Neuroblastoma"
["Kelly_George"]="3_Neuroblastoma"
["Neuroblastoma_NB2"]="3_Neuroblastoma"
["Neuroblastoma_NB1"]="3_Neuroblastoma"
["SHSY5Y_George"]="3_Neuroblastoma"
["HCC827_GR6_K27ac"]="4_Lung"
["HCC827_K27ac"]="4_Lung"
["H2171"]="4_Lung"
["H69"]="4_Lung"
["H82"]="4_Lung"
["GLC16"]="4_Lung"
["PC9_DMSO"]="4_Lung"
["V459"]="5_Colorectal"
["Colo741"]="5_Colorectal"
["HT29"]="5_Colorectal"
["LS174T"]="5_Colorectal"
["DLD1"]="5_Colorectal"
["HCT116"]="5_Colorectal"
["VACO_503"]="5_Colorectal"
["VACO_400"]="5_Colorectal"
["VACO_9M"]="5_Colorectal"
["MM057_Primary_Melanoma"]="6_Melanoma"
["MM001_Primary_Melanoma"]="6_Melanoma"
["MM011_Primary_Melanoma"]="6_Melanoma"
["SKMEL5_Aerts"]="6_Melanoma"
["MM047_Primary_Melanoma"]="6_Melanoma"
["MM034_Primary_Melanoma"]="6_Melanoma"
["MM074_Primary_Melanoma"]="6_Melanoma"
["MM099_Primary_Melanoma"]="6_Melanoma"
["MM031_Primary_Melanoma"]="6_Melanoma"
["MM087_Primary_Melanoma"]="6_Melanoma"
["MM118_Primary_Melanoma"]="6_Melanoma"
["SKMEL5_Tsao"]="6_Melanoma"
["LY4"]="9_Bcelllymphoma"
["DHL6"]="9_Bcelllymphoma"
["LY3"]="9_Bcelllymphoma"
["P265_Primary_DLBCL"]="9_Bcelllymphoma"
["P448_Primary_DLBCL"]="9_Bcelllymphoma"
["TOLEDO"]="9_Bcelllymphoma"
["HBL1"]="9_Bcelllymphoma"
["P286_Primary_DLBCL"]="9_Bcelllymphoma"
["P397_Primary_DLBCL"]="9_Bcelllymphoma"
["PC3"]="11_Other"
["u87"]="8_Glioblastoma"
["Ly1"]="9_Bcelllymphoma"
["MM1S"]="11_Other"
["P493"]="11_Other"
["HeLa"]="11_Other"
["MOLM13"]="11_Other"
["MV411_1"]="11_Other"
["GM12878"]="11_Other"
["HepG2"]="11_Other"
["SEM"]="11_Other"
["K562"]="11_Other"
["A673"]="11_Other"
["panc1"]="10_Pancreatic"
["SKMNC"]="11_Other"
["LnCAP"]="11_Other"
["U2OS"]="11_Other"



["Ly1_Published"]="9_Bcelllymphoma"
["BE2C_only_new"]="3_Neuroblastoma"


["CAL51"]="2_Breast"
["NMC_1015"]="11_Other"
["PFEIFFER"]="9_Bcelllymphoma"
["GBM_S08"]="8_Glioblastoma"
["GBM_R28"]="8_Glioblastoma"
["GBM_P69"]="8_Glioblastoma "
["GBM_2585"]="8_Glioblastoma"
["GBM_2493"]="8_Glioblastoma"
["GBM_B39"]="8_Glioblastoma"
["GBM_IDHmut5661"]="8_Glioblastoma"

["KARPAS_422"]="9_Bcelllymphoma "
["JEKO1"]="11_Other"
["MINO"]="9_Bcelllymphoma "
["797_r1_1"]="11_Other"
["797_r1_2"]="11_Other "
["DOHH2"]="9_Bcelllymphoma "
["GRANTA_519"]="9_Bcelllymphoma"
["OCI_LY7"]="9_Bcelllymphoma"
["REC1"]="9_Bcelllymphoma"
["RS411"]="11_Other"

["MOLM14"]="11_Other "
["HUCCT1"]="11_Other "
["HCC95"]="4_Lung"
["H2009_r1"]="4_Lung"
["H2009_r2"]="4_Lung "
["H2009_empty"]="4_Lung "
["H2009_control"]="4_Lung"
["H358_r1"]="4_Lung"
["H358_r2"]="4_Lung"
["MCF10A"]="2_Breast"

["HPAF2"]="10_Pancreatic"
["PT45P1"]="10_Pancreatic"
["CFPAC1"]="10_Pancreatic"
["MIAPaca2"]="10_Pancreatic"
["CAPAN2"]="10_Pancreatic"
["CAPAN1"]="10_Pancreatic"
["LS180"]="5_Colorectal"
)

declare -A mappedReads=(
["Kelly_George"]="18869861"
["Neuroblastoma_NB1"]="23574467"
["Neuroblastoma_NB2"]="23264476"
["Neuroblastoma_NB3"]="30245069"
["SHSY5Y_Look"]="15711178"
["SKNSH"]="39984235"
["BE2"]="42699008"
["BT_549"]="32269361"
["HCC1954"]="23460453"
["MCF7"]="49590853"
["MDA_MB_468"]="37371985"
["T47D"]="40933662"
["ZR_75_1"]="51230734"
["MDA_MB_231"]="26362317"
["DND41"]="70061045"
["Jurkat_JR1"]="84096380"
["Loucy"]="22106744"
["MOLT4"]="66002939"
["RPMI-8402"]="26857718"
["DU528"]="19975674"
["P12_ICHIKAWA"]="24661051"
["KOPTK1"]="24659520"
["PF382"]="26526052"
["CCRFCEM"]="19010452"
["CUTLL1"]="47213199"
["GLC16"]="19766463"
["H2171"]="27469496"
["H69"]="25212180"
["H82"]="21583673"
["HCT116"]="13558146"
["HT29"]="25529047"
["VACO_400"]="23692725"
["VACO_503"]="32315575"
["VACO_9M"]="24056495"
["LS174T"]="59572391"
["Colo741"]="28635468"
["MM034_Primary_Melanoma"]="26081887"
["MM047_Primary_Melanoma"]="29382328"
["MM087_Primary_Melanoma"]="26622272"
["MM001_Primary_Melanoma"]="35692654"
["MM057_Primary_Melanoma"]="41617461"
["MM099_Primary_Melanoma"]="27737446"
["MM011_Primary_Melanoma"]="29841716"
["MM118_Primary_Melanoma"]="28969080"
["MM031_Primary_Melanoma"]="31237928"
["MM074_Primary_Melanoma"]="25648385"
["P265_Primary_DLBCL"]="55403984"
["P286_Primary_DLBCL"]="59110823"
["DHL6"]="30491662"
["HBL1"]="34501990"
["LY3"]="43050302"
["LY4"]="36179956"
["TOLEDO"]="36191392"
["P397_Primary_DLBCL"]="40787569"
["P448_Primary_DLBCL"]="48991104"
["GM12878"]="9727404"
["HeLa"]="37819790"
["HepG2"]="13691034"
["K562"]="22288821"
["LnCAP"]="10791755"
["MM1S"]="21865735"
["panc1"]="23290160"
["u87"]="35263089"
["U2OS"]="2985498"
["A673"]="16269619"
["SKMNC"]="9105037"
["P493"]="35484967"
["PC3"]="15988995"
["V459"]="27723974"
["SKMEL5_Aerts"]="33549896"
["Ly1_Published"]="45972485"
["BE2C_only_new"]="23004293"
["GBM_S08"]="11335959"
["GBM_R28"]="18950890"
["GBM_P69"]="9747396"
["GBM_2585"]="7861989"
["GBM_2493"]="9482342"
["GBM_B39"]="15422221"
["GBM_IDHmut5661"]="39185001"
["HPAF2"]="19060290"
["PT45P1"]="28729543"
["CFPAC1"]="34989985"
["MIAPaca2"]="42794718"
["CAPAN2"]="31010445"
["CAPAN1"]="26511998"
["CAL51"]="18715581"
["DOHH2"]="82614999"
["GRANTA_519"]="70892996"
["KARPAS_422"]="70098507"
["MINO"]="87446461"
["OCI_LY7"]="52011055"
["PFEIFFER"]="30374544"
["REC1"]="54987313"
["NMC_1015"]="10887594"
["JEKO1"]="93734837"
["797_r1_1"]="29969489"
["RS411"]="44953733"
["MOLM14"]="39883819"
["HUCCT1"]="28742200"
["HCC95"]="2300915"
["H358_r2"]="2954549"
["MCF10A"]="12744869"
["LS180"]="19039216"
)

declare -A seqedReads=(
["Kelly_George"]="23043236"
["Neuroblastoma_NB1"]="31106862"
["Neuroblastoma_NB2"]="32797099"
["Neuroblastoma_NB3"]="39147297"
["SHSY5Y_Look"]="19345964"
["SKNSH"]="47453250"
["BE2"]="50731224"
["BT_549"]="41109524"
["HCC1954"]="32539710"
["MCF7"]="62189406"
["MDA_MB_468"]="47237718"
["T47D"]="49388203"
["ZR_75_1"]="66208885"
["MDA_MB_231"]="30469609"
["DND41"]="86369897"
["Jurkat_JR1"]="103198838"
["Loucy"]="27238838"
["MOLT4"]="85542063"
["RPMI-8402"]="37922025"
["DU528"]="28768087"
["P12_ICHIKAWA"]="35480315"
["KOPTK1"]="31129149"
["PF382"]="38375735"
["CCRFCEM"]="27898698"
["CUTLL1"]="70726859"
["GLC16"]="24988280"
["H2171"]="38432050"
["H69"]="32216787"
["H82"]="27648671"
["HCT116"]="16973132"
["HT29"]="31206210"
["VACO_400"]="31937306"
["VACO_503"]="42398029"
["VACO_9M"]="31329025"
["LS174T"]="75786184"
["Colo741"]="33689497"
["MM034_Primary_Melanoma"]="27355754"
["MM047_Primary_Melanoma"]="31957348"
["MM087_Primary_Melanoma"]="29029938"
["MM001_Primary_Melanoma"]="38410661"
["MM057_Primary_Melanoma"]="44241071"
["MM099_Primary_Melanoma"]="29438764"
["MM011_Primary_Melanoma"]="31711504"
["MM118_Primary_Melanoma"]="31636621"
["MM031_Primary_Melanoma"]="34015402"
["MM074_Primary_Melanoma"]="27100266"
["P265_Primary_DLBCL"]="66889022"
["P286_Primary_DLBCL"]="73499250"
["DHL6"]="36893430"
["HBL1"]="40107247"
["LY3"]="50559122"
["LY4"]="43284313"
["TOLEDO"]="42675131"
["P397_Primary_DLBCL"]="49267368"
["P448_Primary_DLBCL"]="58345069"
["GM12878"]="27190372"
["HeLa"]="53122836"
["HepG2"]="16961623"
["K562"]="39345130"
["LnCAP"]="14030857"
["MM1S"]="27227503"
["panc1"]="28747595"
["u87"]="49908939"
["U2OS"]="4043897"
["A673"]="21104321"
["SKMNC"]="12682100"
["P493"]="45377383"
["PC3"]="19927379"
["V459"]="34998918"
["SKMEL5_Aerts"]="36782970"
["Ly1_Published"]="57326763"
["BE2C_only_new"]="27312937"
["GBM_S08"]="16107115"
["GBM_R28"]="24052078"
["GBM_P69"]="14253114"
["GBM_2585"]="11435871"
["GBM_2493"]="13558847"
["GBM_B39"]="19597537"
["GBM_IDHmut5661"]="51366290"
["HPAF2"]="22487914"
["PT45P1"]="33683475"
["CFPAC1"]="40937650"
["MIAPaca2"]="50663621"
["CAPAN2"]="36888890"
["CAPAN1"]="31738127"
["CAL51"]="21918779"
["DOHH2"]="105608136"
["GRANTA_519"]="93660282"
["KARPAS_422"]="88736132"
["MINO"]="113908416"
["OCI_LY7"]="66052876"
["PFEIFFER"]="39254727"
["REC1"]="72904640"
["NMC_1015"]="14206820"
["JEKO1"]="122498200"
["797_r1_1"]="37042766"
["RS411"]="51479778"
["MOLM14"]="47250198"
["HUCCT1"]="39026392"
["HCC95"]="2883675"
["H358_r2"]="3426790"
["MCF10A"]="14803379"
["LS180"]="28167957"
)


rm MegaStatTable.txt

for file in Kelly_George Neuroblastoma_NB1 Neuroblastoma_NB2 Neuroblastoma_NB3 SHSY5Y_Look SKNSH BE2 BT_549 HCC1954 MCF7 MDA_MB_468 T47D ZR_75_1 MDA_MB_231 DND41 Jurkat_JR1 Loucy MOLT4 RPMI-8402 DU528 P12_ICHIKAWA KOPTK1 PF382 CCRFCEM CUTLL1 GLC16 H2171 H69 H82 HCT116 HT29 VACO_400 VACO_503 VACO_9M LS174T Colo741 MM034_Primary_Melanoma MM047_Primary_Melanoma MM087_Primary_Melanoma MM001_Primary_Melanoma MM057_Primary_Melanoma MM099_Primary_Melanoma MM011_Primary_Melanoma MM118_Primary_Melanoma MM031_Primary_Melanoma MM074_Primary_Melanoma P265_Primary_DLBCL P286_Primary_DLBCL DHL6 HBL1 LY3 LY4 TOLEDO P397_Primary_DLBCL P448_Primary_DLBCL GM12878 HeLa HepG2 K562 LnCAP MM1S panc1 u87 U2OS A673 SKMNC P493 PC3 V459 SKMEL5_Aerts Ly1_Published GBM_S08 GBM_R28 GBM_P69 GBM_2585 GBM_2493 GBM_B39 GBM_IDHmut5661 HPAF2 PT45P1 CFPAC1 MIAPaca2 CAPAN2 CAPAN1 CAL51 DOHH2 GRANTA_519 KARPAS_422 MINO OCI_LY7 PFEIFFER REC1 NMC_1015 JEKO1 797_r1_1  RS411 MOLM14 HUCCT1 HCC95 H358_r2 MCF10A LS180
do
  export totalbpInEnh=$(awk -F\\t '{sum+= $3-$2} END {print sum}' ../Enhancers/$file\_peaks.bed)
  export totalEAI=$(awk -v sample=$file -F\\t '{if($1==sample) print $0}' ../Compile_Mega_EAI_Table/MegaTable_09-15-16.txt | wc -l)
  export nongermlineEAI=$(awk -v sample=$file -F\\t '{if($1==sample && $4 < 3 && $5=="0") print $0}' ../Compile_Mega_EAI_Table/MegaTable_09-15-16.txt | wc -l)

  export mappedReads=${mappedReads[$file]}
  export seqedReads=${seqedReads[$file]}

  echo -e "$file\t${types[$file]}\t$totalbpInEnh\t$totalEAI\t$mappedReads\t$seqedReads\t$nongermlineEAI" >> MegaStatTable.txt
done



sed -i s/Jurkat\_JR1/Jurkat/ MegaStatTable.txt
sed -i s/DU528/DU\.528/ MegaStatTable.txt
sed -i s/PF382/PF\-382/ MegaStatTable.txt
sed -i s/KOPTK1/KOPT\-K1/ MegaStatTable.txt
sed -i s/CCRFCEM/CCRF\-CEM/ MegaStatTable.txt
sed -i s/P12\_ICHIKAWA/P12\-ICHIKAWA/ MegaStatTable.txt
sed -i s/MDA\_MB\_231/MDA\-MB\-231/ MegaStatTable.txt
sed -i s/MDA\_MB\_468/MDA\-MB\-468/ MegaStatTable.txt
sed -i s/ZR\_75\_1/ZR\-75\-1/ MegaStatTable.txt
sed -i s/BT\_549/BT\-549/ MegaStatTable.txt
sed -i s/BE2C\_only/BE2C/ MegaStatTable.txt
sed -i s/SKNSH/SK\-N\-SH/ MegaStatTable.txt
sed -i s/SHSY5Y\_Look/SH\-SY5Y/ MegaStatTable.txt
sed -i s/Kelly\_George/Kelly/ MegaStatTable.txt
sed -i s/HCC827\_K27ac/HCC827/ MegaStatTable.txt
sed -i s/VACO\_400/VACO400/ MegaStatTable.txt
sed -i s/VACO\_9M/VACO9M/ MegaStatTable.txt
sed -i s/VACO\_503/VACO503/ MegaStatTable.txt
sed -i s/\_Primary\_Melanoma/\ Primary\ Melanoma/ MegaStatTable.txt
sed -i s/\_Primary\_DLBCL/\ Primary\ DLBCL/ MegaStatTable.txt
sed -i s/MV411\_1/MV411/ MegaStatTable.txt
sed -i s/Neuroblastoma\_/Neuroblastoma\ / MegaStatTable.txt
sed -i s/SKMEL5\_Aerts/SK\-MEL\-5/ MegaStatTable.txt
sed -i s/SKMNC/SK\-N\-MC/ MegaStatTable.txt
sed -i s/Ly1/LY1/ MegaStatTable.txt
sed -i s/797\_r1\_1/TC797/ MegaStatTable.txt
sed -i s/BE2C\_new/BE2C/ MegaStatTable.txt
sed -i s/CAPAN1/CAPAN\-1/ MegaStatTable.txt
sed -i s/CAPAN2/CAPAN\-2/ MegaStatTable.txt
sed -i s/GBM\_/GBM\ / MegaStatTable.txt
sed -i s/GRANTA\_519/GRANTA\-519/ MegaStatTable.txt
sed -i s/H358\_r2/H358/ MegaStatTable.txt
sed -i s/JEKO1/JeKo1/ MegaStatTable.txt
sed -i s/KARPAS_422/KARPAS\ 422/ MegaStatTable.txt
sed -i s/LY1_Published/LY1/ MegaStatTable.txt
sed -i s/MIAPaca2/MIA\ PaCa2/ MegaStatTable.txt
sed -i s/SAMPLE/DISPLAYSAMPLE/ MegaStatTable.txt
sed -i s/NMC\_1015/1015c/ MegaStatTable.txt
sed -i s/OCI\_LY7/LY7/ MegaStatTable.txt
sed -i s/PT45P1/PT45\-P1/ MegaStatTable.txt
sed -i s/REC1/REC\-1/ MegaStatTable.txt
sed -i s/RS411/RS\;411/ MegaStatTable.txt
sed -i s/V459/V429/ MegaStatTable.txt



export date=$(date +"%m-%d-%y")
sort -k2,2 MegaStatTable.txt > MegaStatTable_$date.txt

###################
###################
### HOW MANY INSERTIONS OVERALL?
###################
###################

wc -l ../Compile_Mega_EAI_Table/MegaTable_09-15-16.txt  # - 1 for header -> 330,878

###################
###################
### HOW MANY INSERTIONS PER  TYPE?
###################
###################

tail -n +2 ../Compile_Mega_EAI_Table/MegaTable_09-15-16.txt | awk -F\\t '{array[$2]+=1} END { for(type in array) print type "\t" array[type]}' | sort -n > CountsPerType.txt

tail -n +2 ../Compile_Mega_EAI_Table/MegaTable_09-15-16.txt | awk -F\\t '{array[$16]+=1 ; types[$16]=$2} END { for(sample in array) print sample "\t" types[sample] "\t" array[sample]}' | sort -t$'\t' -k2,2n -k3,3nr >| CountsPerSample.txt

tail -n +2 ../Compile_Mega_EAI_Table/MegaTable_09-15-16.txt | awk -F\\t '{array[$16]+=1 ; types[$16]=$2} END { for(sample in array) print sample "\t" types[sample] "\t" array[sample]}' | sort -t$'\t' -k2,2nr -k3,3n >| CountsPerSample_flipped.txt

tail -n +2 ../Compile_Mega_EAD_Table/MegaTable_09-19-16.txt | awk -F\\t '{array[$13]+=1 ; types[$13]=$2} END { for(sample in array) print sample "\t" types[sample] "\t" array[sample]}' | sort -t$'\t' -k2,2n -k3,3nr >| DeletionCountsPerSample.txt


###################
###################
### HOW MANY UNIQUE INSERTION LOCI?  UNIQUE INSERTIONS?
###################
###################

tail -n +2 ../Compile_Mega_EAI_Table/MegaTable_09-15-16.txt | awk -F\\t '{print $12 "_" $13}'  | sort | uniq | wc -l # 139343
tail -n +2 ../Compile_Mega_EAI_Table/MegaTable_09-15-16.txt | awk -F\\t '{print $3}'  | sort | uniq | wc -l # 168568


###################
###################
### HOW MANY NON GERMLINE INSERTIONS?
###################
###################
tail -n +2 ../Compile_Mega_EAI_Table/MegaTable_09-15-16.txt | awk -F\\t '{if($4 < 3 && $5=="0") print $3}'  | sort | uniq | wc -l # 139343



###################
###################
### HOW MUCH ENHANCER SPACE PER SAMPLE
###################
###################
sort -t$'\t' -k2,2n -k4,4nr MegaStatTable_09-15-16.txt | awk -F\\t '{print $1 "\t" $2 "\t" $3}' >| Enhancer_bp_per_sample.txt
sort -t$'\t' -k2,2n -k4,4nr MegaStatTable_09-15-16.txt | awk -F\\t '{print $1 "\t" $2 "\t" $3/3400000000*100}' >| Perc_genome_in_enhancers.txt




###################
###################
### HOW MANY BOWTIE-MAPPABLE READS
###################
###################
sort -t$'\t' -k2,2n -k4,4nr MegaStatTable_09-15-16.txt | awk -F\\t '{print $1 "\t" $2 "\t" $5*100/$6}' >| Perc_reads_mappable_w_bowtie.txt




###################
###################
### SIZE DISTRIBUTION PER SAMPLE
###################
###################
cat ../Compile_Mega_EAI_Table/MegaTable_09-15-16.txt | awk -F\\t '{array[$15]+=1} END {for (size in array) print size "\t" array[size]}' | sort -k1,1n | cut -f 2 | tail -n +2 >| EAI_size_dist.txt
cat ../Compile_Mega_EAI_Table/MegaTable_09-15-16.txt | awk -F\\t '{array[$15]+=1} END {for (size in array) print size "\t" array[size]}' | sort -k1,1n | tail -n +2 >| EAI_size_dist_counts.txt


###################
###################
### CROSS-SAMPLE APPEARANCE COUNTS
###################
###################
cat ../Compile_Mega_EAI_Table/MegaTable_09-15-16.txt | awk -F\\t '{print $3 ":" $4}' | sort | uniq | awk -F':' '{array[$2]+=1} END {for (size in array) print size "\t" array[size]}' | sort -k1,1n | tail -n +2 >| Cross_Sample_Appearance_Counts_dist.txt

head -n 5 Cross_Sample_Appearance_Counts_dist.txt | tail -n 3 | awk -F\\t '{sum+=$2} END {print sum}'
head -n 10 Cross_Sample_Appearance_Counts_dist.txt | tail -n 5 | awk -F\\t '{sum+=$2} END {print sum}'
head -n 20 Cross_Sample_Appearance_Counts_dist.txt | tail -n 10 | awk -F\\t '{sum+=$2} END {print sum}'
head -n 40 Cross_Sample_Appearance_Counts_dist.txt | tail -n 20 | awk -F\\t '{sum+=$2} END {print sum}'
head -n 100 Cross_Sample_Appearance_Counts_dist.txt | tail -n 54 | awk -F\\t '{sum+=$2} END {print sum}'



###################
###################
### NON GERMLINE SNPS per SAMPLE
###################
###################
sort -t$'\t' -k2,2n -k4,4nr MegaStatTable_09-15-16.txt | awk -F\\t '{print $1 "\t" $2 "\t" $7}' >| NonGermline_EAI_per_sample.txt


###################
###################
### HOW MANY READ BIASING INSERTIONS PER SAMPLE
###################
###################

tail -n +2 ../Compile_Mega_EAI_Table/MegaTable_09-15-16.txt | awk -F\\t '{
  types[$16]=$2
  overall[$16]+=1
  if($10*2 < $11 && $11 > 3 && $4 < 3 && $5=="0") 
    rbarray[$16]+=1; 
  } END { for(sample in overall) 
    print sample "\t" types[sample] "\t" rbarray[sample] "\t" overall[sample]
  }' | \
  sort -t$'\t' -k2,2n -k4,4nr | awk -F\\t '{print $1 "\t" $2 "\t" $3}' >| ReadBiasingCountsPerSample.txt


tail -n +2 ../Compile_Mega_EAI_Table/MegaTable_09-15-16.txt | awk -F\\t '{
  types[$16]=$2
  overall[$16]+=1
  if($10*2 < $11 && $11 > 3 && $4 < 3 && $5=="0") 
    rbarray[$16]+=1; 
  } END { for(sample in overall) 
    print sample "\t" types[sample] "\t" rbarray[sample] "\t" rbarray[sample]
  }' | \
  sort -t$'\t' -k2,2nr -k4,4n | awk -F\\t '{print $1 "\t" $2 "\t" $3}' >| ReadBiasingCountsPerSample_flip.txt



grep Jurkat ../Compile_Mega_EAI_Table/MegaTable_09-15-16.txt | awk -F\\t '{if($10*2 < $11 && $11 > 3) print $0}' | grep "chr1_477"
grep MOLT ../Compile_Mega_EAI_Table/MegaTable_09-15-16.txt | awk -F\\t '{if($10*2 < $11 && $11 > 3) print $0}' | grep "chr1_477"
grep MOLT ../Compile_Mega_EAI_Table/MegaTable_09-15-16.txt | awk -F\\t '{if($10*2 < $11 && $11 > 3) print $0}' | grep "chr11_339"
###################
###################
### HOW MANY READ BIASING INSERTIONS OVERALL
###################
###################

tail -n +2 ../Compile_Mega_EAI_Table/MegaTable_09-15-16.txt | awk -F\\t '{
  if($10*2 < $11 && $11 > 3 && $4 < 3 && $5=="0") 
      print $0 }'  >| ReadBiasingCountsOverall.txt   # 7220



###################
###################
### WHAT ONCOGENES w/ EAIs
###################
###################

awk -F\\t '{  
  if($10*2 < $11 && $11 > 3 && $4 < 3 && $5=="0") {
    genes[length(genes)+1]=$8
    types[length(genes)+1]=$2

  }
} END  { for(type in genes) print types[type] "\t" genes[type]}' ../Compile_Mega_EAI_Table/MegaTable_09-15-16.txt | grep -v "No" | sort -k1,1n | uniq >| Putative_target_genes.txt



###################
###################
### GERMLINE EAIs BY TYPE
###################
###################

tail -n +2 ../Compile_Mega_EAI_Table/MegaTable_09-15-16.txt | awk -F\\t '{print $3 "\t" $4 "\t" $5}' | sort | uniq >| unique_germline_data.txt
awk -F\\t '{if($2>2 || $3==1) print $0}' unique_germline_data.txt > unique_germline_data.areGermline.txt   # 57209
awk -F\\t '{if($3==1) print $0}' unique_germline_data.txt >  unique_germline_data.aredbSNP.txt # 50140
awk -F\\t '{if($3==0) print $0}' unique_germline_data.txt >  unique_germline_data.notdbSNP.txt
awk -F\\t '{if($2>2) print $0}' unique_germline_data.notdbSNP.txt > unique_germline_data.notdbSNP.recurrent.txt
awk -F\\t '{if($2>2) print $0}' unique_germline_data.txt > unique_germline_data.areRecurrent.txt  # 20865
awk -F\\t '{if($3==1 && $2 < 3) print $0}' unique_germline_data.txt > unique_germline_data.dbSNPnotrecurr.txt
awk -F\\t '{if($3==1 && $2 > 2) print $0}' unique_germline_data.txt > unique_germline_data.dbSNPrecurr.txt
awk -F\\t '{if($3==0 && $2 < 3) print $0}' unique_germline_data.txt > unique_germline_data.notdbSNPnotrecurr.txt
awk -F\\t '{if($2 < 3) print $0}' unique_germline_data.txt > unique_germline_data.notrecurr.txt


tail -n +2 ../Compile_Mega_EAI_Table/MegaTable_09-15-16.txt | awk -F\\t '{print $3 "\t" $4 "\t" $5}' | sort | uniq >| dbSNP_EAI_total.txt
sort -t$'\t' -k2,2n -k4,4nr ../Compile_Mega_EAI_Table/MegaTable_09-15-16.txt | awk -F\\t '{if($5==1) print $0}' >| dbSNP_EAI_total.txt


###################
###################
### MAX SIZE READ-CALLED AND CONTIG-CALLED
###################
###################

cat ../Cross_Cancer_Sharing_smartEnh/*read.inPeaks.bed | awk -F\\t '{print length($4)}' | sort -k1,1n | tail
cat ../Cross_Cancer_Sharing_smartEnh/*sca.inPeaks.bed | awk -F\\t '{print length($4) "\t" $4}' | sort -k1,1n | tail

