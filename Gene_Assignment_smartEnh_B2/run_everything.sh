export CTCFchip="/archive/tony/TONY2/Young/Homo.sapiens/T-ALL/Jurkat/CTCF/20150124_3461/20150124_3461_hg19.sorted.bam"
export CTCFctrl="/archive/tony/TONY2/Young/Homo.sapiens/T-ALL/Jurkat/CTCF/20150124_3462/20150124_3462_hg19.sorted.bam"

bsub macs -t $CTCFchip -c $CTCFctrl --keep-dup=1 -p 1e-9 -n CTCFchip

bedtools pairtobed -type both -a TableS2A.txt -b CTCFchip_peaks.bed  | cut -f 1-8 | sort -uk 1,8 | awk -F\\t '{if($8 < 0.2) print $0}' >| TableS2A.txt.withCTCF

awk -F\\t '{print $1 "\t" $2 "\t" $6 "\t" $1 ":" $2 "-" $3 "==" $4 ":" $5 "-" $6 "\t" $7 "\t" $8}' TableS2A.txt.test | sort -k1,1 -k2,2n -k3,3n > TableS2A.txt.withCTCF.washu

export Jurkat_CC_loops="TableS2A.txt.withCTCF.washu"

export genes="../CANCERS/hg19_refseq.ucsc"
awk -F\\t '{if($4=="+")
	      print $3 "\t" $5 "\t" $5 "\t" $2 "\t" $13
	    else if($4=="-")
	      print $3 "\t" $6 "\t" $6 "\t" $2 "\t" $13}' $genes > hg19_refseq.TSS.bed
export proms="hg19_refseq.TSS.bed"
grep -w -f Oncogenes.txt hg19_refseq.TSS.bed > hg19_refseq.TSS.oncogenes.bed
export oncoproms="hg19_refseq.TSS.oncogenes.bed"

###############################
###############################

# 
intersectBed -loj -a $Jurkat_CC_loops -b $proms > TSS_in_CCloops.loj.txt
intersectBed -loj -a $Jurkat_CC_loops -b $oncoproms > OncoTSS_in_CCloops.loj.txt
awk -F\\t '{print $4}' TSS_in_CCloops.loj.txt | sort | uniq > Unique_CCloopIDs.txt

rm TSS_in_CCloops.list.txt
while read p
do
  echo $p > name
  grep $p TSS_in_CCloops.loj.txt | awk -F\\t '{print $11}' | sort | uniq | sed ':a;N;$!ba;s/\n/ /g' > temp
  paste name temp >> TSS_in_CCloops.list.txt
done< Unique_CCloopIDs.txt

rm OncoTSS_in_CCloops.list.txt
while read p
do
  echo $p > name
  grep $p OncoTSS_in_CCloops.loj.txt | awk -F\\t '{print $11}' | sort | uniq | sed ':a;N;$!ba;s/\n/ /g' > temp
  paste name temp >> OncoTSS_in_CCloops.list.txt
done< Unique_CCloopIDs.txt

###############################
###############################



for file in Kelly_George Neuroblastoma_NB1 Neuroblastoma_NB2 Neuroblastoma_NB3 SHSY5Y_Look SKNSH BE2 BT_549 HCC1954 MCF7 MDA_MB_468 T47D ZR_75_1 MDA_MB_231 DND41 Jurkat_JR1 Loucy MOLT4 RPMI-8402 DU528 P12_ICHIKAWA KOPTK1 PF382 CCRFCEM CUTLL1 GLC16 H2171 H69 H82 HCT116 HT29 VACO_400 VACO_503 VACO_9M LS174T Colo741 MM034_Primary_Melanoma MM047_Primary_Melanoma MM087_Primary_Melanoma MM001_Primary_Melanoma MM057_Primary_Melanoma MM099_Primary_Melanoma MM011_Primary_Melanoma MM118_Primary_Melanoma MM031_Primary_Melanoma MM074_Primary_Melanoma P265_Primary_DLBCL P286_Primary_DLBCL DHL6 HBL1 LY3 LY4 TOLEDO P397_Primary_DLBCL P448_Primary_DLBCL GM12878 HeLa HepG2 K562 LnCAP MM1S panc1 u87 U2OS A673 SKMNC P493 PC3 V459 SKMEL5_Aerts Ly1_Published GBM_S08 GBM_R28 GBM_P69 GBM_2585 GBM_2493 GBM_B39 GBM_IDHmut5661 HPAF2 PT45P1 CFPAC1 MIAPaca2 CAPAN2 CAPAN1 CAL51 DOHH2 GRANTA_519 KARPAS_422 MINO OCI_LY7 PFEIFFER REC1 NMC_1015 JEKO1 797_r1_1  RS411 MOLM14 HUCCT1 HCC95 H358_r2 MCF10A LS180
do
  echo $file
# 
  sed s/\_/\\t/g ../Cross_Cancer_Sharing_smartEnh/$file.inPeaks.unique.txt > $file.ins.bed
  intersectBed -loj -a $file.ins.bed -b $Jurkat_CC_loops > test1
  intersectBed -loj -a $file.ins.bed -b $Jurkat_CC_loops | awk -F\\t '{print $1 ":" $2 "-" $3 "_" $4 "\t" $5 "\t" $6 "\t" $7 "\t" $8 "\t" $9 "\t" $10 "\t" $11}' > test1a
  awk -F\\t '{print $1 ":" $2 "-" $3 "_" $4}' test1 | sort | uniq > test1b

  awk -F\\t '{print $1 "\t" $2 "\t" $3 "\t" $4 "\t" $5 "\t" $6 "\t" $7 "\t" $5-$4}' test1a | sort -nk 8 > test1a2
  while read p
  do
    grep -m 1 $p test1a2  
  done < test1b > test2
  perl vlookup.pl test2 4 TSS_in_CCloops.list.txt 0 1 $file.all.txt
  perl vlookup.pl test2 4 OncoTSS_in_CCloops.list.txt 0 1 $file.onco.txt

  awk -F\\t '{print $9}' $file.onco.txt > temp
  paste $file.all.txt temp > $file.tmp
  mv $file.tmp $file.all.txt
done

grep "chr" *all.txt | awk -F\\t '{if($5 != "-1" && $5 != "XXX") print $0}' >| oncogene_summary.txt

cat *.all.txt >| all_cat.txt

