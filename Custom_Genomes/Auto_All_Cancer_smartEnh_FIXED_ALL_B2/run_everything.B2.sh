declare -A k27ac=(
["797_r1_1"]="/archive/tony/TONY2/Kharchenko/Homo.sapiens/NUT.Midline.Carcinoma/TC797/H3K27Ac/20150716_4449/20150716_4449_hg19.sorted.bam"
["A673"]="/archive/tony/TONY2/Aryee/Homo.sapiens/Ewing.Sarcoma/A673/H3K27Ac/20141104_3703/20141104_3703_hg19.sorted.bam"
["BE2"]="/archive/tony/TONY2/Young/Homo.sapiens/Neuroblastoma/BE2C/H3K27Ac/20150206_3512/20150206_3512_hg19.sorted.bam"
["BT_549"]="/archive/tony/TONY2/Zhao/Homo.sapiens/Breast.Cancer/BT-549/H3K27Ac/20140911_3051/20140911_3051_hg19.sorted.bam"
["CAL51"]="/archive/tony/TONY2/Polyak/Homo.sapiens/Breast.Cancer/CAL51/H3K27Ac/20150610_4445/20150610_4445_hg19.sorted.bam"
["CAPAN1"]="/archive/tony/TONY2/Natoli/Homo.sapiens/Pancreatic.Ductal.Adenocarcinoma/CAPAN1/H3K27Ac/20160119_4488/20160119_4488_hg19.sorted.bam"
["CAPAN2"]="/archive/tony/TONY2/Natoli/Homo.sapiens/Pancreatic.Ductal.Adenocarcinoma/CAPAN2/H3K27Ac/20160119_4487/20160119_4487_hg19.sorted.bam"
["CCRFCEM"]="/archive/tony/TONY2/Tom.Look/Homo.sapiens/T-ALL/CCRF-CEM/H3K27Ac/20150407_3730/20150407_3730_hg19.sorted.bam"
["CFPAC1"]="/archive/tony/TONY2/Natoli/Homo.sapiens/Pancreatic.Ductal.Adenocarcinoma/CFPAC1/H3K27Ac/20160119_4486/20160119_4486_hg19.sorted.bam"
["Colo741"]="/archive/tony/TONY2/Young/Homo.sapiens/Colon.Carcinoma/Colo741/H3K27Ac/03142013_D1WLRACXX_3.GATCAG/03142013_D1WLRACXX_3.GATCAG_hg19.sorted.bam"
["CUTLL1"]="/archive/tony/TONY2/Liu/Homo.sapiens/T-ALL/CUTLL1/H3K27Ac/20131227_4252/20131227_4252_hg19.sorted.bam"
["DHL6"]="/archive/tony/TONY2/Bradner/Homo.sapiens/DLBCL/DHL6/H3K27Ac/20140821_3163/20140821_3163_hg19.sorted.bam"
["DND41"]="/archive/tony/TONY2/Broad/Homo.sapiens/T-ALL/DND41/H3K27Ac/GSM1003462_Comb/GSM1003462_Comb_hg19.sorted.bam"
["DOHH2"]="/archive/tony/TONY2/Bernstein/Homo.sapiens/B-cell.lymphoma/DOHH2/H3K27Ac/20150804_4459/20150804_4459_hg19.sorted.bam "
["DU528"]="/archive/tony/TONY2/Tom.Look/Homo.sapiens/T-ALL/DU.528/H3K27Ac/20150407_3721/20150407_3721_hg19.sorted.bam"
["GBM_2493"]="/archive/tony/TONY2/Mischel/Homo.sapiens/Glioblastoma/Glioblastoma.biopsy/H3K27Ac/20150915_4468/20150915_4468_hg19.sorted.bam"
["GBM_2585"]="/archive/tony/TONY2/Mischel/Homo.sapiens/Glioblastoma/Glioblastoma.biopsy/H3K27Ac/20150915_4467/20150915_4467_hg19.sorted.bam"
["GBM_B39"]="/archive/tony/TONY2/Mischel/Homo.sapiens/Glioblastoma/Glioblastoma.biopsy/H3K27Ac/20150915_4466/20150915_4466_hg19.sorted.bam"
["GBM_IDHmut5661"]="/archive/tony/TONY2/Mischel/Homo.sapiens/Glioblastoma/Glioblastoma.biopsy/H3K27Ac/20150915_4465/20150915_4465_hg19.sorted.bam"
["GBM_P69"]="/archive/tony/TONY2/Mischel/Homo.sapiens/Glioblastoma/Glioblastoma.biopsy/H3K27Ac/20150915_4464/20150915_4464_hg19.sorted.bam "
["GBM_R28"]="/archive/tony/TONY2/Mischel/Homo.sapiens/Glioblastoma/Glioblastoma.biopsy/H3K27Ac/20150915_4463/20150915_4463_hg19.sorted.bam"
["GBM_S08"]="/archive/tony/TONY2/Mischel/Homo.sapiens/Glioblastoma/Glioblastoma.biopsy/H3K27Ac/20150915_4462/20150915_4462_hg19.sorted.bam"
["GLC16"]="/archive/tony/TONY2/Wong-DFCI/Homo.sapiens/Small.Cell.Lung.Carcinoma/GLC16/H3K27Ac/20140124_3132/20140124_3132_hg19.sorted.bam"
["GM12878"]="/archive/tony/TONY2/Bernstein/Homo.sapiens/Lymphoblastoid/GM12878/H3K27Ac/GSM733771_SRR227633/GSM733771_SRR227633_hg19.sorted.bam"
["GRANTA_519"]="/archive/tony/TONY2/Bernasconi/Homo.sapiens/B-cell.lymphoma/GRANTA-519/H3K27Ac/20150804_4458/20150804_4458_hg19.sorted.bam"
["H2171"]="/archive/tony/TONY2/Young/Homo.sapiens/Small.Cell.Lung.Carcinoma/H2171/H3K27Ac/11232011_C08B8ACXX_2.TAGCTT/11232011_C08B8ACXX_2.TAGCTT_hg19.sorted.bam"
["H358_r2"]="/archive/tony/TONY2/Meyerson/Homo.sapiens/Non-small.cell.lung.carcinoma/H358/H3K27Ac/20151026_4477/20151026_4477_hg19.sorted.bam"
["H69"]="/archive/tony/TONY2/Wong-DFCI/Homo.sapiens/Small.Cell.Lung.Carcinoma/NCI-H69/H3K27Ac/20140131_3137/20140131_3137_hg19.sorted.bam"
["H82"]="/archive/tony/TONY2/Wong-DFCI/Homo.sapiens/Small.Cell.Lung.Carcinoma/NCI-H82/H3K27Ac/20140124_3122/20140124_3122_hg19.sorted.bam"
["HBL1"]="/archive/tony/TONY2/Bradner/Homo.sapiens/DLBCL/HBL1/H3K27Ac/20140821_3165/20140821_3165_hg19.sorted.bam"
["HCC1954"]="/archive/tony/TONY2/Bing.Ren/Homo.sapiens/Breast.Cancer/HCC1954/H3K27Ac/GSM721136_Comb/GSM721136_Comb_hg19.sorted.bam"
["HCC95"]="/archive/tony/TONY2/Meyerson/Homo.sapiens/Non-small.cell.lung.carcinoma/HCC95/H3K27Ac/20151026_4474/20151026_4474_hg19.sorted.bam"
["HCT116"]="/archive/tony/TONY2/ENCODE.UCSC/Homo.sapiens/Colon.Carcinoma/HCT-116/H3K27Ac/GSM945853_SRR504923/GSM945853_SRR504923_hg19.sorted.bam"
["HeLa"]="/archive/tony/TONY2/Bernstein/Homo.sapiens/Cervical.carcinoma/HeLa-S3/H3K27Ac/GSM733684_Comb/GSM733684_Comb_hg19.sorted.bam"
["HepG2"]="/archive/tony/TONY2/Bernstein/Homo.sapiens/Hepatocellular.carcinoma/HepG2/H3K27Ac/GSM733743_Comb/GSM733743_Comb_hg19.sorted.bam"
["HPAF2"]="/archive/tony/TONY2/Natoli/Homo.sapiens/Pancreatic.Ductal.Adenocarcinoma/HPAF2/H3K27Ac/20160119_4485/20160119_4485_hg19.sorted.bam"
["HT29"]="/archive/tony/TONY2/Young/Homo.sapiens/Colon.Carcinoma/HT29/H3K27Ac/03142013_D1WLRACXX_2.TTAGGC/03142013_D1WLRACXX_2.TTAGGC_hg19.sorted.bam"
["HUCCT1"]="/archive/tony/TONY2/Calogero/Homo.sapiens/Cholangeocarcinoma/HuCCT1/H3K27Ac/20151001_4470/20151001_4470_hg19.sorted.bam "
["IMR90"]="/archive/tony/TONY2/NIH.Roadmap.Epigenomics.Project/Homo.sapiens/Fetal.lung.fibroblast/IMR90/H3K27Ac/GSM469966_Comb/GSM469966_Comb_hg19.sorted.bam"
["JEKO1"]="/archive/tony/TONY2/Bernstein/Homo.sapiens/Mantle.Cell.Lymphoma/JEKO-1/H3K27Ac/20150804_4457/20150804_4457_hg19.sorted.bam"
["Jurkat_JR1"]="/archive/tony/TONY2/Young/Homo.sapiens/T-ALL/Jurkat/H3K27Ac/11252013_C2GUGACXX_2.CGATGT/11252013_C2GUGACXX_2.CGATGT_hg19.sorted.bam"
["K562"]="/archive/tony/TONY2/Bernstein/Homo.sapiens/Leukemia/K562/H3K27Ac/GSM733656_Comb/GSM733656_Comb_hg19.sorted.bam"
["KARPAS_422"]="/archive/tony/TONY2/Bernstein/Homo.sapiens/B-cell.lymphoma/KARPAS-422/H3K27Ac/20150804_4456/20150804_4456_hg19.sorted.bam "
["Kelly_George"]="/archive/tony/TONY2/George/Homo.sapiens/Neuroblastoma/Kelly/H3K27Ac/20141121_3372/20141121_3372_hg19.sorted.bam"
["KOPTK1"]="/archive/tony/TONY2/Young/Homo.sapiens/T-ALL/KOPTK1/H3K27Ac/20150602_3928/20150602_3928_hg19.sorted.bam"
["LnCAP"]="/archive/tony/TONY2/Xiang-Dong.Fu/Homo.sapiens/Adenocarcinoma/LnCaP/H3K27Ac/GSM686937_SRR122327/GSM686937_SRR122327_hg19.sorted.bam"
["Loucy"]="/archive/tony/TONY2/Young/Homo.sapiens/T-ALL/Loucy/H3K27Ac/03282014_C4AUGACXX_4.AGTTCC/03282014_C4AUGACXX_4.AGTTCC_hg19.sorted.bam"
["LS174T"]="/archive/tony/TONY2/Batlle/Homo.sapiens/Colon.Carcinoma/LS174T/H3K27Ac/20140520_2941/20140520_2941_hg19.sorted.bam"
["LS180"]="/archive/tony/TONY2/Genentech/Homo.sapiens/Colon.Carcinoma/LS180/H3K27Ac/20160128_4489/20160128_4489_hg19.sorted.bam"
["Ly1_Published"]="/archive/tony/TONY2/Bradner/Homo.sapiens/DLBCL/LY1/H3K27Ac/GSM1133646_Comb/GSM1133646_Comb_hg19.sorted.bam"
["LY3"]="/archive/tony/TONY2/Bradner/Homo.sapiens/DLBCL/LY3/H3K27Ac/20140821_3167/20140821_3167_hg19.sorted.bam"
["LY4"]="/archive/tony/TONY2/Bradner/Homo.sapiens/DLBCL/LY4/H3K27Ac/20140821_3169/20140821_3169_hg19.sorted.bam"
["MCF10A"]="/archive/tony/TONY2/Enuka/Homo.sapiens/Breast.Cancer/MCF10A/H3K27Ac/20151218_4479/20151218_4479_hg19.sorted.bam"
["MCF7"]="/archive/tony/TONY2/ENCODE.UCSC/Homo.sapiens/Breast.Cancer/MCF7/H3K27Ac/GSM945854_Comb/GSM945854_Comb_hg19.sorted.bam"
["MDA_MB_231"]="/archive/tony/TONY2/Coetzee/Homo.sapiens/Breast.Cancer/MDA-MB-231/H3K27Ac/20131201_3588/20131201_3588_hg19.sorted.bam"
["MDA_MB_468"]="/archive/tony/TONY2/Zhao/Homo.sapiens/Breast.Cancer/MDA-MB-468/H3K27Ac/20140911_3052/20140911_3052_hg19.sorted.bam"
["MIAPaca2"]="/archive/tony/TONY2/Natoli/Homo.sapiens/Pancreatic.Ductal.Adenocarcinoma/MIA.Paca.2/H3K27Ac/20160119_4484/20160119_4484_hg19.sorted.bam"
["MINO"]="/archive/tony/TONY2/Bernstein/Homo.sapiens/B-cell.lymphoma/MINO/H3K27Ac/20150804_4455/20150804_4455_hg19.sorted.bam "
["MM001_Primary_Melanoma"]="/archive/tony/TONY2/Aerts/Homo.sapiens/Melanoma/Primary.Melanoma.Culture/H3K27Ac/20150305_4240/20150305_4240_hg19.sorted.bam"
["MM011_Primary_Melanoma"]="/archive/tony/TONY2/Aerts/Homo.sapiens/Melanoma/Primary.Melanoma.Culture/H3K27Ac/20150305_4241/20150305_4241_hg19.sorted.bam"
["MM031_Primary_Melanoma"]="/archive/tony/TONY2/Aerts/Homo.sapiens/Melanoma/Primary.Melanoma.Culture/H3K27Ac/20150305_4242/20150305_4242_hg19.sorted.bam"
["MM034_Primary_Melanoma"]="/archive/tony/TONY2/Aerts/Homo.sapiens/Melanoma/Primary.Melanoma.Culture/H3K27Ac/20150305_4243/20150305_4243_hg19.sorted.bam"
["MM047_Primary_Melanoma"]="/archive/tony/TONY2/Aerts/Homo.sapiens/Melanoma/Primary.Melanoma.Culture/H3K27Ac/20150305_4244/20150305_4244_hg19.sorted.bam"
["MM057_Primary_Melanoma"]="/archive/tony/TONY2/Aerts/Homo.sapiens/Melanoma/Primary.Melanoma.Culture/H3K27Ac/20150305_4245/20150305_4245_hg19.sorted.bam"
["MM074_Primary_Melanoma"]="/archive/tony/TONY2/Aerts/Homo.sapiens/Melanoma/Primary.Melanoma.Culture/H3K27Ac/20150305_4246/20150305_4246_hg19.sorted.bam"
["MM087_Primary_Melanoma"]="/archive/tony/TONY2/Aerts/Homo.sapiens/Melanoma/Primary.Melanoma.Culture/H3K27Ac/20150305_4247/20150305_4247_hg19.sorted.bam"
["MM099_Primary_Melanoma"]="/archive/tony/TONY2/Aerts/Homo.sapiens/Melanoma/Primary.Melanoma.Culture/H3K27Ac/20150305_4248/20150305_4248_hg19.sorted.bam"
["MM118_Primary_Melanoma"]="/archive/tony/TONY2/Aerts/Homo.sapiens/Melanoma/Primary.Melanoma.Culture/H3K27Ac/20150305_4249/20150305_4249_hg19.sorted.bam"
["MM1S"]="/archive/tony/TONY2/Young/Homo.sapiens/Multiple.myeloma/Plasma.cell/H3K27Ac/12022011_B0A1WABXX_3.TGACCA/12022011_B0A1WABXX_3.TGACCA_hg19.sorted.bam"
["MOLM14"]="/archive/tony/TONY2/Shair/Homo.sapiens/Acute.Myeloid.Leukemia/MOLM14/H3K27Ac/20151021_4469/20151021_4469_hg19.sorted.bam "
["MOLT4"]="/archive/tony/TONY2/Young/Homo.sapiens/T-ALL/MOLT4/H3K27Ac/20150318_3594/20150318_3594_hg19.sorted.bam"
["Neuroblastoma_NB1"]="/archive/tony/TONY2/George/Homo.sapiens/Neuroblastoma/Primary.Neuroblastoma/H3K27Ac/20141121_3373/20141121_3373_hg19.sorted.bam"
["Neuroblastoma_NB2"]="/archive/tony/TONY2/George/Homo.sapiens/Neuroblastoma/Primary.Neuroblastoma/H3K27Ac/20141121_3374/20141121_3374_hg19.sorted.bam"
["Neuroblastoma_NB3"]="/archive/tony/TONY2/George/Homo.sapiens/Neuroblastoma/Kelly/H3K27Ac/20141121_3371/20141121_3371_hg19.sorted.bam"
["NMC_1015"]="/archive/tony/TONY2/Kharchenko/Homo.sapiens/NUT.Midline.Carcinoma/1015c/H3K27Ac/20150716_4448/20150716_4448_hg19.sorted.bam"
["OCI_LY7"]="/archive/tony/TONY2/Bernstein/Homo.sapiens/B-cell.lymphoma/OCI-LY7/H3K27Ac/20150804_4454/20150804_4454_hg19.sorted.bam"
["P12_ICHIKAWA"]="/archive/tony/TONY2/Tom.Look/Homo.sapiens/T-ALL/P12-ICHIKAWA/H3K27Ac/20150407_3715/20150407_3715_hg19.sorted.bam"
["P265_Primary_DLBCL"]="/archive/tony/TONY2/Bradner/Homo.sapiens/DLBCL/Primary.tumor/H3K27Ac/20131210_3176/20131210_3176_hg19.sorted.bam"
["P286_Primary_DLBCL"]="/archive/tony/TONY2/Bradner/Homo.sapiens/DLBCL/Primary.tumor/H3K27Ac/20131210_3178/20131210_3178_hg19.sorted.bam"
["P397_Primary_DLBCL"]="/archive/tony/TONY2/Bradner/Homo.sapiens/DLBCL/Primary.tumor/H3K27Ac/GSM1254214_SRR1020518/GSM1254214_SRR1020518_hg19.sorted.bam"
["P448_Primary_DLBCL"]="/archive/tony/TONY2/Bradner/Homo.sapiens/DLBCL/Primary.tumor/H3K27Ac/GSM1254216_SRR1020520/GSM1254216_SRR1020520_hg19.sorted.bam"
["P493"]="/archive/tony/TONY2/Young/Homo.sapiens/B-cell.lymphoma/P493-6/H3K27Ac/02242012_C06TJACXX_3.GCCAAT/02242012_C06TJACXX_3.GCCAAT_hg19.sorted.bam"
["panc1"]="/archive/tony/TONY2/UC.Davis/Homo.sapiens/Pancreatic.carcinoma/Panc-1/H3K27Ac/GSM818826_SRR353689/GSM818826_SRR353689_hg19.sorted.bam"
["PC3"]="/archive/tony/TONY2/Clark/Homo.sapiens/Prostate.cancer/PC3/H3K27Ac/20140605_3589/20140605_3589_hg19.sorted.bam"
["PF382"]="/archive/tony/TONY2/Tom.Look/Homo.sapiens/T-ALL/PF-382/H3K27Ac/20150407_3727/20150407_3727_hg19.sorted.bam"
["PFEIFFER"]="/archive/tony/TONY2/Bernstein/Homo.sapiens/B-cell.lymphoma/Pfeiffer/H3K27Ac/20150804_4453/20150804_4453_hg19.sorted.bam"
["PT45P1"]="/archive/tony/TONY2/Natoli/Homo.sapiens/Pancreatic.Ductal.Adenocarcinoma/PT45P1/H3K27Ac/20160119_4483/20160119_4483_hg19.sorted.bam"
["REC1"]="/archive/tony/TONY2/Bernstein/Homo.sapiens/B-cell.lymphoma/REC-1/H3K27Ac/20150804_4452/20150804_4452_hg19.sorted.bam"
["RPMI-8402"]="/archive/tony/TONY2/Young/Homo.sapiens/T-ALL/RPMI-8402/H3K27Ac/11132012_64WVDAAXX_4/11132012_64WVDAAXX_4_hg19.sorted.bam"
["RS411"]="/archive/tony/TONY2/Wu/Homo.sapiens/Acute.lymphocytic.leukemia/RS411/H3K27Ac/20151001_4471/20151001_4471_hg19.sorted.bam"
["SHSY5Y_Look"]="/archive/tony/TONY2/Tom.Look/Homo.sapiens/Acute.lymphocytic.leukemia/SHSY5Y/H3K27Ac/20141124_3332/20141124_3332_hg19.sorted.bam"
["SKMEL5_Aerts"]="/archive/tony/TONY2/Aerts/Homo.sapiens/Melanoma/SK-MEL-5/H3K27Ac/20150305_3803/20150305_3803_hg19.sorted.bam"
["SKMNC"]="/archive/tony/TONY2/Aryee/Homo.sapiens/Ewing.Sarcoma/SKMNC/H3K27Ac/20141104_4279/20141104_4279_hg19.sorted.bam"
["SKNSH"]="/archive/tony/TONY2/Young/Homo.sapiens/Neuroblastoma/SK-N-SH/H3K27Ac/20150206_3517/20150206_3517_hg19.sorted.bam"
["T47D"]="/archive/tony/TONY2/Zhao/Homo.sapiens/Breast.Cancer/T47D/H3K27Ac/20140911_3053/20140911_3053_hg19.sorted.bam"
["TOLEDO"]="/archive/tony/TONY2/Bradner/Homo.sapiens/DLBCL/TOLEDO/H3K27Ac/20140821_3171/20140821_3171_hg19.sorted.bam"
["U2OS"]="/archive/tony/TONY2/Eilers/Homo.sapiens/Osteosarcoma/U2OS/H3K27Ac/20140710_3079/20140710_3079_hg19.sorted.bam"
["u87"]="/archive/tony/TONY2/Young/Homo.sapiens/Glioblastoma/U87/H3K27Ac/01172012_C0EGAACXX_4.TGACCA/01172012_C0EGAACXX_4.TGACCA_hg19.sorted.bam"
["V459"]="/archive/tony/TONY2/Scacheri/Homo.sapiens/Colon.Carcinoma/V429/H3K27Ac/GSM883681_SRR424634/GSM883681_SRR424634_hg19.sorted.bam"
["VACO_400"]="/archive/tony/TONY2/Scacheri/Homo.sapiens/Colon.Carcinoma/V400/H3K27Ac/GSM883684_SRR424637/GSM883684_SRR424637_hg19.sorted.bam"
["VACO_503"]="/archive/tony/TONY2/Scacheri/Homo.sapiens/Colon.Carcinoma/V503/H3K27Ac/GSM883682_SRR424635/GSM883682_SRR424635_hg19.sorted.bam"
["VACO_9M"]="/archive/tony/TONY2/Scacheri/Homo.sapiens/Colon.Carcinoma/V9M/H3K27Ac/GSM883683_SRR424636/GSM883683_SRR424636_hg19.sorted.bam"
["ZR_75_1"]="/archive/tony/TONY2/Zhao/Homo.sapiens/Breast.Cancer/ZR-75-1/H3K27Ac/20140911_3054/20140911_3054_hg19.sorted.bam"
)


echo -e ">HEADER" > header.txt

rm ReadBiasingInsertions_and_AsstdGenes.txt AllInsertions_and_AsstdGenes.txt nondbSNPInsertions_and_AsstdGenes.txt
touch ReadBiasingInsertions_and_AsstdGenes.txt AllInsertions_and_AsstdGenes.txt nondbSNPInsertions_and_AsstdGenes.txt

for file in "${!k27ac[@]}"
do
  echo $file
  bsub -sp 100 -o /dev/null bamToFastq -i ${k27ac[$file]} -fq /lab/solexa_young/scratch/BJA_SENI_overflow/FASTQs/$file.fastq
  mkdir /lab/solexa_young/scratch/BJA_SENI_overflow/Custom_Genomes_v4/$file.BWT.REF /lab/solexa_young/scratch/BJA_SENI_overflow/Custom_Genomes_v4/$file.BWT.INS $file.align.REF $file.align.INS INS_LOCUS.$file REF_LOCUS.$file

  sed s/\_/\\t/g ../../Cross_Cancer_Sharing_smartEnh/$file.inPeaks.unique.txt > $file.ins.txt
  export readLen=$(samtools view ${k27ac[$file]} | head -n 1 | awk -F\\t '{print length($10)}' )
  export numIns=$(wc -l $file.ins.txt | awk -F' ' '{print $1}')

  export lineNum=1


  echo -e "

  export lineNum=1


  while read p
  do  
      export readLen=$readLen
      export file=$file

      echo \$p | awk -F' ' -v readLen=$readLen '{print \$1 \"\t\" \$2-readLen \"\t\" \$2-1}' > $file.UPSTREAM.B2.bed3
      echo \$p | awk -F' ' -v readLen=$readLen '{print \$1 \"\t\" \$3-1 \"\t\" \$3+readLen}' > $file.DOWNSTREAM.B2.bed3
      echo \$p | awk -F' ' '{print \$4}' > $file.INSSEQ.B2.txt

      fastaFromBed -fi /nfs/genomes/human_gp_feb_09_no_random/fasta_whole_genome/hg19.fa -bed $file.UPSTREAM.B2.bed3 -fo $file.UPSTREAM.B2.fasta
      fastaFromBed -fi /nfs/genomes/human_gp_feb_09_no_random/fasta_whole_genome/hg19.fa -bed $file.DOWNSTREAM.B2.bed3 -fo $file.DOWNSTREAM.B2.fasta

      tail -n +2 $file.UPSTREAM.B2.fasta > $file.UPSTREAM.B2.seq
      tail -n +2 $file.DOWNSTREAM.B2.fasta > $file.DOWNSTREAM.B2.seq

      paste $file.UPSTREAM.B2.seq $file.INSSEQ.B2.txt $file.DOWNSTREAM.B2.seq | sed s/\\\\\\\t//g > $file.INS_LOCUS.\$lineNum.B2.seq
      paste $file.UPSTREAM.B2.seq $file.DOWNSTREAM.B2.seq | sed s/\\\\\\\t//g > $file.REF_LOCUS.\$lineNum.B2.seq

      cat header.txt $file.INS_LOCUS.\$lineNum.B2.seq > $file.INS_LOCUS.\$lineNum.B2.fasta
      cat header.txt $file.REF_LOCUS.\$lineNum.B2.seq > $file.REF_LOCUS.\$lineNum.B2.fasta
   
      bowtie-build $file.INS_LOCUS.\$lineNum.B2.fasta /lab/solexa_young/scratch/BJA_SENI_overflow/Custom_Genomes_v4/$file.BWT.INS/INS_LOCUS.\$lineNum
      bowtie-build $file.REF_LOCUS.\$lineNum.B2.fasta /lab/solexa_young/scratch/BJA_SENI_overflow/Custom_Genomes_v4/$file.BWT.REF/REF_LOCUS.\$lineNum

      gzip -f $file.INS_LOCUS.\$lineNum.B2.fasta $file.REF_LOCUS.\$lineNum.B2.fasta 

      mv $file.INS_LOCUS.\$lineNum.B2.fasta.gz INS_LOCUS.$file/INS_LOCUS.\$lineNum.B2.fasta.gz
      mv $file.REF_LOCUS.\$lineNum.B2.fasta.gz REF_LOCUS.$file/REF_LOCUS.\$lineNum.B2.fasta.gz
 
      rm $file.INS_LOCUS.\$lineNum.B2.seq $file.REF_LOCUS.\$lineNum.B2.seq

    lineNum=\$((\$lineNum+1))
    echo \$lineNum
  done < $file.ins.txt

  " > $file.tmp.sh

#################################


  export lineNum=1
  rm $file.align.sh

  while read p
  do
    echo -e "
    bowtie --chunkmbs 256 --best --strata -m 1 -n 2 --al $file.align.INS/$file.$lineNum.fastq -S /lab/solexa_young/scratch/BJA_SENI_overflow/Custom_Genomes_v4/$file.BWT.INS/INS_LOCUS.$lineNum $file.INSalignable.fastq > /dev/null
    bowtie --chunkmbs 256 --best --strata -m 1 -n 2 --al $file.align.REF/$file.$lineNum.fastq -S /lab/solexa_young/scratch/BJA_SENI_overflow/Custom_Genomes_v4/$file.BWT.REF/REF_LOCUS.$lineNum $file.REFalignable.fastq > /dev/null

    " >> $file.align.sh
    lineNum=$(($lineNum+1))
  done < $file.ins.txt

#################################

  export lineNum=1

  rm $file.compress.sh

  while read p
  do
    echo -e "
    gzip -f /lab/solexa_young/scratch/BJA_SENI_overflow/Custom_Genomes_v4/$file.BWT.INS/INS_LOCUS.$lineNum*ebwt 
    gzip -f /lab/solexa_young/scratch/BJA_SENI_overflow/Custom_Genomes_v4/$file.BWT.REF/REF_LOCUS.$lineNum*ebwt 
    gzip -f $file.align.INS/$file.$lineNum.fastq
    gzip -f $file.align.REF/$file.$lineNum.fastq
    " >> $file.compress.sh

    lineNum=$(($lineNum+1))
    echo $lineNum
  done < $file.ins.txt

###########################

  export lineNum=1

  rm $file.calculate.sh
    echo -e "
    rm counts.$file.txt\n touch counts.$file.txt" > $file.calculate.sh

  while read p
  do
    echo -e "
    export InsMapped=\$(zcat $file.align.INS/$file.$lineNum.fastq.gz | wc -l | awk -F' ' '{print $1}')
    let InsMapped=\$InsMapped/4
    InsMapped=\$(echo \$InsMapped)
    
    export RefMapped=\$(zcat $file.align.REF/$file.$lineNum.fastq.gz | wc -l | awk -F' ' '{print $1}')
    let RefMapped=\$RefMapped/4
    RefMapped=\$(echo \$RefMapped)

    echo -e "$lineNum\\t\$RefMapped\\t\$InsMapped\\t$p" >> counts.$file.txt
    " >> $file.calculate.sh


    lineNum=$(($lineNum+1))
#     echo $lineNum
  done < $file.ins.txt


#################################
#################################
#################################

  bsub -sp 100 bash $file.tmp.sh
  bsub gzip -f INS_LOCUS.$file/*fasta REF_LOCUS.$file/*fasta
  

  zcat INS_LOCUS.$file/*fasta.gz > $file.ALL_INS.fasta
  zcat REF_LOCUS.$file/*fasta.gz > $file.ALL_REF.fasta

  bowtie-build $file.ALL_INS.fasta $file.ALL_INS
  bowtie-build $file.ALL_REF.fasta $file.ALL_REF

  sleep 1

  bsub -sp 10 -q special -n 4 -R "span[hosts=1]" bowtie --chunkmbs 256 --best --strata -m $numIns -n 2 -p 4 --al $file.INSalignable.fastq -S $file.ALL_INS /lab/solexa_young/scratch/BJA_SENI_overflow/FASTQs/$file.fastq \> /dev/null
  bsub -o /dev/null -sp 10 -q special -n 4 -R "span[hosts=1]" bowtie --chunkmbs 256 --best --strata -m $numIns -n 2 -p 4 --al $file.REFalignable.fastq -S $file.ALL_REF /lab/solexa_young/scratch/BJA_SENI_overflow/FASTQs/$file.fastq \> /dev/null


  bsub -q special -sp 100 bash $file.align.sh
  bsub -q special bash $file.compress.sh
  bsub bash $file.calculate.sh


done